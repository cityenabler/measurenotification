# Measure Notification

Spring boot project that receives measure notification via API and exposes it using a Stomp websocket

## Deploy

The **cityenabler/measurenotification** docker image is available in DockerHub.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning
We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Antonino Sirchia** - *Development team leader* - [antonino.sirchia@eng.it](mailto:antonino.sirchia@eng.it)
* **Filippo Giuffrida** - *Development team member* - [filippo.giuffrida@eng.it](mailto:filippo.giuffrida@eng.it)
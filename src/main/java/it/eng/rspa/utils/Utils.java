package it.eng.rspa.utils;

import java.util.Arrays;


public class Utils {
	
	
	/**
	 * Check is a string is present in a list of string
	 * 
	 * @param str
	 * @return
	 */
	public static boolean bagOfWords(String str){
	    String[] words = {
	    		"auth_method",
	    		"auth_pwd",
	    		"auth_user",
	    		"dataformat_protocol", 
	    		"dateModified", 
	    		"device_owner", 
	    		"mobile_device",
	    		"opType",
	    		"organization",
	    		"retrieve_data_mode",
	    		"screenId",
	    		"transport_protocol",
	    		"TimeInstant"
	    		};  
	    
	    return (Arrays.asList(words).contains(str));
	}
	

}

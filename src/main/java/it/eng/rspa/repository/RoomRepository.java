package it.eng.rspa.repository;

import it.eng.rspa.model.Room;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public class RoomRepository{
	
	private static Map<Room, Boolean> rooms = new HashMap<Room, Boolean>();
	
	public Map<Room, Boolean> getRooms(){
		return rooms;
	}
	
	public void initRoom(String service, String servicepath, String deviceId){
		
		//System.out.println(service +" "+ servicepath+" "+ deviceId);
		Room r = new Room(service, servicepath, deviceId);
		rooms.put(r, true);
	}
	
}

package it.eng.rspa.controller;


import it.eng.rspa.model.Room;
import it.eng.rspa.repository.RoomRepository;
import it.eng.rspa.utils.Utils;
import it.eng.rspa.websocket.SocketManager;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


@RestController
@RequestMapping(path="/measures") 
public class RestApis {
	
	@Autowired
	private RoomRepository roomRepo;
	
	@Autowired
	private SocketManager socket;
	
	
	@RequestMapping(method=RequestMethod.POST, value="/receive")
	@ResponseStatus(code=HttpStatus.OK)
	public void onPong(@RequestBody String notificationBody, 
						@RequestHeader("fiware-service") String service, 
						@RequestHeader("fiware-servicepath") String servicepath){
		
		
		
		String parsedServicepath = servicepath.startsWith("/") ? servicepath.substring(1) : servicepath;
		ObjectMapper objectMapperInput = new ObjectMapper();
		ObjectMapper objectMapperOutput = new ObjectMapper();
		JsonNode nodeOutput = objectMapperOutput.createObjectNode();
		ObjectNode outputObjectNode = ((ObjectNode) nodeOutput);
		
		String deviceID = "";
		
		//System.out.println("notificationBody "+notificationBody);

		JsonNode rootNode = null;
		try {
			rootNode = objectMapperInput.readTree(notificationBody);
			
			JsonNode data = rootNode.get("data");
			
			JsonNode dataEl = data.get(0);
			
			
			Iterator<Entry<String, JsonNode>> firstLevel = dataEl.fields();
			
			 while (firstLevel.hasNext()) {
				 
				 Entry<String, JsonNode> firstElem = firstLevel.next();
				 
			        
				 if (!firstElem.getValue().isObject()){

					 //System.out.println("firstElem.getKey() "+firstElem.getKey() + " firstElem.getValue() "+firstElem.getValue());
					 
					 if (firstElem.getKey().equals("id"))
						 deviceID=firstElem.getValue().asText();
				 }else{
					 
					 String objValue = firstElem.getValue().get("value").asText();
					 
					 boolean isMetadata = Utils.bagOfWords(firstElem.getKey());
					 
					 if (!isMetadata){
//						 System.out.println(firstElem.getKey()+" objValue "+objValue);
						 outputObjectNode.put(firstElem.getKey(), objValue);
					 }
					 
					 
				 }
			 }
			 
		} catch (IOException  e) {
			e.printStackTrace();
		}
		
		
		
		Room r = new Room(service, parsedServicepath, deviceID); 
		
		// If there is any Room for this device, it is created.
		// This is a new device
		if(!roomRepo.getRooms().containsKey(r)){
			roomRepo.initRoom(service, parsedServicepath, deviceID);
		}
		
		String roompk = r.buildRoomID();
		
		
		/*
		 * JSON Output example -- outputObjectNode.toString()
		 * {"dB":"11","location":"0, 0","noise":"text1"}
		 * 
		 */
		
		
		socket.send(roompk,deviceID, outputObjectNode.toString());
		
	}
	
	/*
	 * JSON input example
	 * 
	 * {
	"subscriptionId": "5d7f5fd8e7cef356221d392f",
	"data": [{
		"id": "FilTest1",
		"type": "Device",
		"Pressure": {
			"type": "text",
			"value": " ",
			"metadata": {}
		},
		"TimeInstant": {
			"type": "ISO8601",
			"value": " ",
			"metadata": {}
		},
		"auth_method": {
			"type": "Text",
			"value": "none",
			"metadata": {}
		},
		"auth_pwd": {
			"type": "Text",
			"value": "pwd1",
			"metadata": {}
		},
		"auth_user": {
			"type": "Text",
			"value": "aaa",
			"metadata": {}
		},
		"cmd1_info": {
			"type": "commandResult",
			"value": " ",
			"metadata": {}
		},
		"cmd1_status": {
			"type": "commandStatus",
			"value": "UNKNOWN",
			"metadata": {}
		},
		"dataformat_protocol": {
			"type": "Text",
			"value": "JSON",
			"metadata": {}
		},
		"dateModified": {
			"type": "DateTime",
			"value": "2019-09-16T12:09:43.00Z",
			"metadata": {}
		},
		"device_owner": {
			"type": "Text",
			"value": "7da190b8-6373-45a3-b95a-d3601ac8a2c9",
			"metadata": {}
		},
		"location": {
			"type": "geo:point",
			"value": "13.265,37.499444",
			"metadata": {}
		},
		"mobile_device": {
			"type": "Text",
			"value": "false",
			"metadata": {}
		},
		"opType": {
			"type": "Text",
			"value": "ready",
			"metadata": {}
		},
		"organization": {
			"type": "Text",
			"value": "91785315-5d5e-400b-ac84-1b382ea5ebae",
			"metadata": {}
		},
		"retrieve_data_mode": {
			"type": "Text",
			"value": "push",
			"metadata": {}
		},
		"screenId": {
			"type": "Text",
			"value": "FilTest1",
			"metadata": {}
		},
		"temperature": {
			"type": "text",
			"value": "21",
			"metadata": {}
		},
		"temperature_1": {
			"type": "text",
			"value": "11",
			"metadata": {}
		},
		"transport_protocol": {
			"type": "Text",
			"value": "HTTP",
			"metadata": {}
		}
	}]
}

 */
	
	
	
	

}

package it.eng.rspa.model;

public class Room{
	private String service;
	private String servicepath;
	private String deviceId;
	
	public Room(String service, String servicepath, String deviceId) {
		this.service = service;
		this.servicepath = servicepath.startsWith("/") ? servicepath.substring(1) : servicepath;
		this.deviceId = deviceId;
	}
	
	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getServicepath() {
		return servicepath;
	}

	public void setServicepath(String servicepath) {
		this.servicepath = servicepath;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public String buildRoomID() {
		return service + "/" + servicepath + "/" + deviceId;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((servicepath == null) ? 0 : servicepath.hashCode());
		result = prime * result + ((service == null) ? 0 : service.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		if (servicepath == null) {
			if (other.servicepath != null)
				return false;
		} else if (!servicepath.equals(other.servicepath))
			return false;
		if (service == null) {
			if (other.service != null)
				return false;
		} else if (!service.equals(other.service))
			return false;
		return true;
	}	
	
	
}

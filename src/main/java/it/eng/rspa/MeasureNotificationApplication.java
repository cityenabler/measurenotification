package it.eng.rspa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeasureNotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeasureNotificationApplication.class, args);
	}

}

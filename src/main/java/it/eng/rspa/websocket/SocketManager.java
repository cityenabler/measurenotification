package it.eng.rspa.websocket;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SocketManager {
	
	@Autowired
	private MeasureNotificationController measureNotificationController;
	
	public void send(String roomid, String deviceid, String measure){
		try{ measureNotificationController.send(roomid, deviceid, measure); }
		catch(Exception e){
			e.printStackTrace();
		}
	}
}

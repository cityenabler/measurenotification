package it.eng.rspa.websocket;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class MeasureNotificationController {

	@Autowired
    private SimpMessagingTemplate template;
	
	@MessageMapping("/measurenotifications")
	public void send(String roomid, String deviceid, String measure) throws Exception {
        //Long timeinmillis = GregorianCalendar.getInstance().getTimeInMillis();
        
        
        this.template.convertAndSend("/topic/"+roomid, measure );   
    }
	
}
